package dao;

import bean.CamaBean;
import bean.HabitacionBean;
import bean.PersonaBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilerias.ConexionMysql;

public class HabitacionDao {

    public static void main(String[] args) {
        System.out.println(consultarCamas(1));
    }
    
    
    public static List<HabitacionBean> consultarHabitacion() {
        List<HabitacionBean> listaHabitacion = new ArrayList();
        String consulta = "SELECT idhabitacion,nombreHabitacion,numeroCuartos FROM habitacion WHERE habitacion.idEstado = 1 order by idhabitacion desc;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HabitacionBean habitacion = new HabitacionBean();
                habitacion.setId(rs.getInt(1));
                habitacion.setNombre(rs.getString(2));
                habitacion.setNumeroCamas(rs.getInt(3));
                listaHabitacion.add(habitacion);
            }
            rs.close();
            ps.close();
            conexion.close();
        } catch (SQLException sqle) {
            System.out.println("consultarHabitacion()" + sqle);
        }
        return listaHabitacion;
    }

    public static int recuperarIdHabitacion(HabitacionBean habitacion) {
        int idHabitacion = 0;
        String consulta = "SELECT max(idhabitacion) FROM habitacion;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            rs.next();
            idHabitacion = rs.getInt(1);
            rs.close();
            ps.close();
            conexion.close();
        } catch (SQLException sqle) {
            System.out.println("recuperarIdHabitacion() " + sqle);
        }
        return idHabitacion;
    }
    
    public static List<CamaBean> consultarCamas(int idHabitacion) {
        List<CamaBean> listaCama = new ArrayList();
            String consulta = "SELECT idCama,descripcion,idhabitacion FROM cama WHERE idhabitacion= ?;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setInt(1, idHabitacion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CamaBean cama = new CamaBean();
                cama.setId(rs.getInt(1));
                cama.setDescripcion(rs.getString(2));
                cama.getHabitacion().setId(rs.getInt(3));
                listaCama.add(cama);
            }
            rs.close();
            ps.close();
            conexion.close();
        } catch (SQLException sqle) {
            System.out.println("consultarHabitacion()" + sqle);
        }
        return listaCama;
    }

    public static boolean insertarHabitacion(HabitacionBean habitacion) {
        boolean respuesta = false;
        String consulta = "INSERT INTO habitacion(nombreHabitacion,numeroCuartos,idEstado) VALUES(?,?,?);";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setString(1, habitacion.getNombre());
            ps.setInt(2, habitacion.getNumeroCamas());
            ps.setInt(3, 1);
            respuesta = (ps.executeUpdate() > 0);
            ps.close();
            conexion.close();
        } catch (SQLException sql) {
            System.out.println("registarrPersona()" + sql);
        }
        return respuesta;
    }

    public static boolean insertarCama(int idHabitacion, CamaBean cama) {
        boolean respuesta = false;
        String consulta = "INSERT INTO cama(descripcion,idHabitacion,idEstado) VALUES (?,?,?);";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setString(1, cama.getDescripcion());
            ps.setInt(2, idHabitacion);
            ps.setInt(3, 1);
            respuesta = (ps.executeUpdate() > 0);
            ps.close();
            conexion.close();
        } catch (SQLException sql) {
            System.out.println("registrarCama()" + sql);
        }
        return respuesta;
    }

    public static boolean modificarHabitacion(HabitacionBean habitacion) {
        boolean respuesta = false;
        String consulta = "UPDATE habitacion SET nombreHabitacion = ?, numeroCuartos = ?,habitacion WHERE idhabitacion = ?;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setString(1, habitacion.getNombre());
            ps.setInt(2, habitacion.getNumeroCamas());
            ps.setInt(3, 1);
            ps.setInt(4, habitacion.getId());
            respuesta = (ps.executeUpdate() > 0);
            ps.close();
            conexion.close();
        } catch (SQLException sql) {
            System.out.println("registarrPersona()" + sql);
        }
        return respuesta;
    }
    
    public static boolean modificarCama(CamaBean cama) {
        boolean respuesta = false;
        String consulta = "UPDATE cama SET descripcion = ?, idEstado = ? WHERE idCama = ?;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setString(1, cama.getDescripcion());
            ps.setInt(2, cama.getEstado().getId());
            ps.setInt(3, cama.getId());
            respuesta = (ps.executeUpdate() > 0);
            ps.close();
            conexion.close();
        } catch (SQLException sql) {
            System.out.println("registarrPersona()" + sql);
        }
        return respuesta;
    }

}