
package bean;

import java.util.List;

public class PersonaBean {
    private int id;
    private String nombre;
    private String apellidos;
    private SexoBean sexo;
    private TipoPersonaBean tipoPersona;
    private List<CamaBean> cama;
    private AdministradorBean admin;
    private CamaBean camaO;

    public PersonaBean() {
        this.tipoPersona = new TipoPersonaBean();
        this.sexo = new SexoBean();
        this.admin = new AdministradorBean();
        this.camaO = new CamaBean();
    }

    public PersonaBean(String nombre, String apellidos, SexoBean sexo, TipoPersonaBean tipoPersona) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.tipoPersona = tipoPersona;
    }
    
    public CamaBean getCamaO() {
        return camaO;
    }

    public void setCamaO(CamaBean camaO) {
        this.camaO = camaO;
    }

     public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public SexoBean getSexo() {
        return sexo;
    }

    public void setSexo(SexoBean sexo) {
        this.sexo = sexo;
    }

    public TipoPersonaBean getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(TipoPersonaBean tipoPersona) {
        this.tipoPersona = tipoPersona;
    }
    
    public List<CamaBean> getCama() {
        return cama;
    }

    public void setCama(List<CamaBean> cama) {
        this.cama = cama;
    }

    @Override
    public String toString() {
        return "PersonaBean{" + "nombre=" + getNombre() + ", apellidos=" + getApellidos() + ", sexo=" + getSexo() + ", tipoPersona=" + getTipoPersona() + '}';
    }   

    public AdministradorBean getAdmin() {
        return admin;
    }

    public void setAdmin(AdministradorBean admin) {
        this.admin = admin;
    }

}
