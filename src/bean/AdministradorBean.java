
package bean;

public class AdministradorBean {
    private int id;
    private String user;
    private String password;
    private PersonaBean persona;
    private EstadoBean estado;

    public AdministradorBean() {
        
        this.estado = new EstadoBean();
    }

    public AdministradorBean(int id, String user, String password, PersonaBean persona, EstadoBean estado) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.persona = persona;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PersonaBean getPersona() {
        return persona;
    }

    public void setPersona(PersonaBean persona) {
        this.persona = persona;
    }

    public EstadoBean getEstado() {
        return estado;
    }

    public void setEstado(EstadoBean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "AdministradorBean{" + "id=" + id + ", user=" + user + ", password=" + password + ", persona=" + persona + ", estado=" + estado + '}';
    }
    
}
