
package bean;

public class EstadoBean {
    private int id;
    private String descripcion;

    public EstadoBean() {
    }

    public EstadoBean(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "EstadoBean{" + "id=" + id + ", descripcion=" + descripcion + '}';
    }
}
