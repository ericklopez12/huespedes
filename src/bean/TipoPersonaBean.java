
package bean;

import java.util.List;

public class TipoPersonaBean {
    private int id;
    private String descripcion;
    private List<PersonaBean> listaPersonas;

    public TipoPersonaBean() {
    }

    public TipoPersonaBean(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public TipoPersonaBean(int id, String descripcion, List<PersonaBean> listaPersonas) {
        this.id = id;
        this.descripcion = descripcion;
        this.listaPersonas = listaPersonas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<PersonaBean> getListaPersonas() {
        return listaPersonas;
    }

    public void setListaPersonas(List<PersonaBean> listaPersonas) {
        this.listaPersonas = listaPersonas;
    }

    @Override
    public String toString() {
        return "TipoPersonaBean{" + "id=" + id + ", descripcion=" + descripcion + ", listaPersonas=" + listaPersonas + '}';
    }
    
    
    
    
}
