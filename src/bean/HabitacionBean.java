package bean;

public class HabitacionBean {

    private int id;
    private String nombre;
    private EstadoBean estado;
    private CamaBean cama;
    private int numeroCamas;

  public int getNumeroCamas() {
        return numeroCamas;
    }

    public void setNumeroCamas(int numeroCamas) {
        this.numeroCamas = numeroCamas;
    }
    
    public CamaBean getCama() {
        return cama;
    }

    public void setCama(CamaBean cama) {
        this.cama = cama;
    }

    public HabitacionBean() {
        this.estado = new EstadoBean();
    }

    public HabitacionBean(int id, String nombre, EstadoBean estado) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public EstadoBean getEstado() {
        return estado;
    }

    public void setEstado(EstadoBean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return  id + " - " +nombre;
    }
}
