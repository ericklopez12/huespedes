
package bean;

import java.util.List;

public class CamaBean {

    private int id;
    private String descripcion;
    private HabitacionBean habitacion;
    private List<PersonaBean> persona;
    private EstadoBean estado;
    private int personaB;

    public CamaBean() {
    this.habitacion = new HabitacionBean();
    }

    public CamaBean(int id, String descripcion, HabitacionBean habitacion) {
        this.id = id;
        this.descripcion = descripcion;
        this.habitacion = habitacion;
    }
    
    public EstadoBean getEstado() {
        return estado;
    }

    public void setEstado(EstadoBean estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public HabitacionBean getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(HabitacionBean habitacion) {
        this.habitacion = habitacion;
    }

     public List<PersonaBean> getPersona() {
        return persona;
    }

    public void setPersona(List<PersonaBean> persona) {
        this.persona = persona;
    }

    @Override
    public String toString() {
        return "CamaBean{" + "id=" + id + ", descripcion=" + descripcion + ", habitacion=" + habitacion + '}';
    }

    public int getPersonaB() {
        return personaB;
    }

    public void setPersonaB(int personaB) {
        this.personaB = personaB;
    }

   }
