package dao;

import bean.SexoBean;
import bean.TipoPersonaBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilerias.ConexionMysql;

public class ListasDao {

    public static List<SexoBean> consultarSexo() {
        List<SexoBean> listaSexo = new ArrayList();
        String consulta = "SELECT idSexo,descripcion FROM sexo";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                SexoBean sexo = new SexoBean();
                sexo.setId(rs.getInt(1));
                sexo.setDescripcion(rs.getString(2));
                listaSexo.add(sexo);
            }
            rs.close();
            ps.close();
            conexion.close();
        } catch (SQLException sqle) {
            System.out.println("consultaSexo()" + sqle);
        }
        return listaSexo;
    }

    public static List<TipoPersonaBean> consultarTipoPersona() {
        List<TipoPersonaBean> listaTipoPersona = new ArrayList();
        String consulta = "SELECT idTipoPersona,descripcion FROM tipo_persona";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TipoPersonaBean tipoPersona = new TipoPersonaBean();
                tipoPersona.setId(rs.getInt(1));
                tipoPersona.setDescripcion(rs.getString(2));
                listaTipoPersona.add(tipoPersona);
            }
            rs.close();
            ps.close();
            conexion.close();
        } catch (SQLException sqle) {
            System.out.println("consultaSexo()" + sqle);
        }
        return listaTipoPersona;
    }
}
