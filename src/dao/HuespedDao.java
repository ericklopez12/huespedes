package dao;

import bean.PersonaBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilerias.ConexionMysql;

public class HuespedDao {

    public static List<PersonaBean> consultarHuespedes() {
        List<PersonaBean> listaPersonas = new ArrayList();
        String consulta = "select id,nombre,apellidos,idHuesped,fechaEntrada from persona as p inner join huesped as h on p.id = h.idPersona order by idHuesped desc;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PersonaBean persona = new PersonaBean();
                persona.setId(rs.getInt(1));
                persona.setNombre(rs.getString(2));
                persona.setApellidos(rs.getString(3));
                persona.getCamaO().setId(rs.getInt(4));
                persona.getCamaO().setDescripcion(rs.getDate(5) + "");
                listaPersonas.add(persona);
            }
            rs.close();
            ps.close();

            conexion.close();

        } catch (SQLException sqle) {
            System.out.println("consultarPersonas()" + sqle);
        }
        return listaPersonas;
    }
}