package dao;

import bean.PersonaBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilerias.ConexionMysql;

public class PersonaDao {
    
    public static boolean validarUsuario(String usuario, String password) {
        boolean respuesta = false;
        String consulta = "select idAdmin from administrador where idEstado = 1 AND user = ? AND pass = ?;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            System.out.println(usuario + " " +password);
            ps.setString(1, usuario);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            rs.next();
            respuesta =  rs.getInt(1)!=0;
            rs.close();
            ps.close();
            conexion.close();
        } catch (SQLException sqle) {
            System.out.println("consultarPersonas()" + sqle);
        }
        return respuesta;
    }
    
    public static boolean registrarUsuario(PersonaBean persona) {
        boolean respuesta = false;
        String consulta = "INSERT INTO administrador(idPersona,idEstado,User,pass) values (?,?,?,?);";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setInt(1, persona.getId());
            ps.setInt(2, 1);
            ps.setString(3, persona.getAdmin().getUser());
            ps.setString(4, persona.getAdmin().getPassword());
            respuesta = (ps.executeUpdate() > 0);
            ps.close();
            conexion.close();
        } catch (SQLException sql) {
            System.out.println("registarrPersona()" + sql);
        }
        return respuesta;
    }

    public static List<PersonaBean> consultarPersonasGeneral() {
        List<PersonaBean> listaPersonas = new ArrayList();
        String consulta = "SELECT id,nombre,apellidos,p.idSexo,s.descripcion,tp.idTipoPersona,tp.descripcion FROM persona as p JOIN sexo as s ON p.idSexo = s.idSexo JOIN tipo_persona as tp ON tp.idTipoPersona = p.idTipoPersona;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PersonaBean persona = new PersonaBean();
                persona.setId(rs.getInt(1));
                persona.setNombre(rs.getString(2));
                persona.setApellidos(rs.getString(3));
                persona.getSexo().setId(rs.getInt(4));
                persona.getSexo().setDescripcion(rs.getString(5));
                persona.getTipoPersona().setId(rs.getInt(6));
                persona.getTipoPersona().setDescripcion(rs.getString(7));
                listaPersonas.add(persona);
            }
            rs.close();
            ps.close();
            conexion.close();

        } catch (SQLException sqle) {
            System.out.println("consultarPersonas()" + sqle);
        }
        return listaPersonas;
    }

    public static List<PersonaBean> consultarPersonasEspecifico(int idTipoPersona) {
        List<PersonaBean> listaPersonas = new ArrayList();
        String consulta ="SELECT id,nombre,apellidos,p.idSexo,s.descripcion,tp.idTipoPersona,tp.descripcion FROM persona as p JOIN sexo as s ON p.idSexo = s.idSexo JOIN tipo_persona as tp ON tp.idTipoPersona = p.idTipoPersona WHERE tp.idTipoPersona = ? order by id desc;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setInt(1, idTipoPersona);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PersonaBean persona = new PersonaBean();
                persona.setId(rs.getInt(1));
                persona.setNombre(rs.getString(2));
                persona.setApellidos(rs.getString(3));
                persona.getSexo().setId(rs.getInt(4));
                persona.getSexo().setDescripcion(rs.getString(5));
                persona.getTipoPersona().setId(rs.getInt(6));
                persona.getTipoPersona().setDescripcion(rs.getString(7));
                listaPersonas.add(persona);
            }
            rs.close();
            ps.close();
            conexion.close();

        } catch (SQLException sqle) {
            System.out.println("consultarPersonas()" + sqle);
        }
        return listaPersonas;
    }

    public static int consultarPersona() {
        int idPersona = 0;
        List<PersonaBean> listaPersonas = new ArrayList();
        String consulta = "SELECT max(id) FROM persona;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            rs.next();
            idPersona = rs.getInt(1);
            rs.close();
            ps.close();
            conexion.close();

        } catch (SQLException sqle) {
            System.out.println("consultarPersonas()" + sqle);
        }
        return idPersona;
    }

    public static boolean modificarPersona(PersonaBean persona) {
        boolean respuesta = false;
        String consulta = "UPDATE persona SET nombre = ? ,apellidos = ? ,idSexo = ? ,idTipoPersona = ? WHERE id = ?;";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setString(1, persona.getNombre());
            ps.setString(2, persona.getApellidos());
            ps.setInt(3, persona.getSexo().getId());
            ps.setInt(4, persona.getTipoPersona().getId());
            ps.setInt(5, persona.getId());
            respuesta = (ps.executeUpdate() > 0);
            ps.close();
            conexion.close();
        } catch (SQLException sql) {
            System.out.println("registarrPersona()" + sql);
        }
        return respuesta;
    }

    public static boolean registrarPersona(PersonaBean persona) {
        boolean respuesta = false;
        String consulta = "INSERT INTO persona (nombre,apellidos,idSexo,idTipoPersona) VALUES (?,?,?,?);";
        try {
            Connection conexion = ConexionMysql.getConnection();
            PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setString(1, persona.getNombre());
            ps.setString(2, persona.getApellidos());
            ps.setInt(3, persona.getSexo().getId());
            ps.setInt(4, persona.getTipoPersona().getId());
            respuesta = (ps.executeUpdate() > 0);
            ps.close();
            conexion.close();
        } catch (SQLException sql) {
            System.out.println("registarrPersona()" + sql);
        }
        return respuesta;
    }
   
}
