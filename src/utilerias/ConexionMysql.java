package utilerias;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConexionMysql {

    private static String direccionIP;
    private static String nombreBaseDatos;
    private static String usuario;
    private static String contraseña;
    private static String puertoServicio;
    private static ResourceBundle proedadesBD;

    public static Connection getConnection() throws SQLException {
        try {

            //class.forName("com.mysql.jdbc.Driver");
            //class.forName("org.postgresql.jdbc.Driver");
            //class.forName("oracle.jdbc.driver.OracleDriver");
            //class.forName("org.firebirdsql.jdbc.FBDriver");
            Class.forName("com.mysql.jdbc.Driver");
            //Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (proedadesBD == null) {
            proedadesBD = ResourceBundle.getBundle("mysql_data");//nombre de archivo properti
            direccionIP = proedadesBD.getString("ip"); //direccion IP
            nombreBaseDatos = proedadesBD.getString("db_name");
            usuario = proedadesBD.getString("user");
            contraseña = proedadesBD.getString("password");
            puertoServicio = proedadesBD.getString("port");

        }
        String urlBaseDeDatos = "jdbc:mysql://" + direccionIP + "/" + nombreBaseDatos;
        return DriverManager.getConnection(urlBaseDeDatos, usuario, contraseña);

    }

    public static void main(String[] args) throws SQLException {

        if (ConexionMysql.getConnection() != null) {
            System.out.println("Conexion establecido ");
        } else {
            System.out.println(" no se realizo la conexion a la base de datos");
        }

    }

}
